﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="WebApplication1.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <img src="~/Images/coffee1.png" runat="server" class="imgLeft" />
    <h3>Espreso</h3>
    <p> Espreso (ital. espresso) jeste vrsta napitka od kafe italijanskog porekla. 
        Dobija se prolaskom vode pod velikim pritiskom kroz mlevenu, usitnjenu kafu. 
        Espreso je uglavnom gušći od kafe koja se pravi drugim metodama, ima veću koncentraciju suspendovanih i rastvorenih čvrstih materija,
        a na vrhu ima kremu (pena sa kremastom konzistencijom) </p>
    <img src="~/Images/coffee2.png" runat="server" class="imgRight" />
    <h3>Late</h3>
    <p> Kafa late (ital. caffè latte; ili samo late, ital. latte) je vrsta kafenog napitka sa toplim mlekom. 
        Postoji i ledena kafa late (ital. iced caffè latte; ili samo ledeni late, ital. iced latte). 
        Kafa late je italijanskog porekla gde se naziva caffè e latte (bukvalno prevedeno „kafa i mleko”, tj. kafa sa mlekom). 
        Late je popularan u mnogim zemljama, mada ne u svojoj izvornoj, italijanskoj, varijanti.</p>
   <img id="Img1" runat="server" src="~/Images/coffee3.png" class="imgLeft" />
    <h3>Kapućino</h3>
    <p> Kapućino – lider među specijalno pripremljenim napicima od kafe. 
        Veliki procenat kafe koji se popije u svetu, pa i kod nas, svakako je u obliku kapućina, ali kako zapravo znati i prepoznati šta jeste, 
        a šta nije pravi kapućino? Čak i stručnjaci polemišu o tome kako bi trebalo da izgreda i kakav bi ukus trebalo da ima pravi kapućino.
        Jedni kažu da je stvar samo u količini, na pari zagrejanog, mleka koja se sipa u espresso;
        drugi pak misle da bi pravi kapućino trebalo da ima tri nivoa (espresso, toplo mleko i gustu mlečnu penu na vrhu), 
        ali treći kažu da bi konzumacija kapućina trebalo da bude ista od prvog do poslednjeg gutljaja.
        </p>
</asp:Content>
