﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public partial class Coffee
{
    public int id { get; set; }
    public string name { get; set; }
    public string type { get; set; }
    public string price { get; set; }
    public string roast { get; set; }
    public string country { get; set; }
    public string image { get; set; }
    public string review { get; set; }

    public Coffee(int id, string name, string type, string price, string roast, string country, string image, string review)
    {
        this.id = id;
        this.name = name;
        this.type = type;
        this.price = price;
        this.roast = roast;
        this.country = country;
        this.image = image;
        this.review = review;
    }
}


